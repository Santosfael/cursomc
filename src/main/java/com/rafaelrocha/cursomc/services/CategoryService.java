package com.rafaelrocha.cursomc.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rafaelrocha.cursomc.domain.Category;
import com.rafaelrocha.cursomc.repositories.CategoryRepository;

@Service
public class CategoryService {

	@Autowired
	private CategoryRepository repository;
	
	public Category search(Integer id) {
		Optional<Category> obj = repository.findById(id);
		return obj.orElse(null);
	}
}
